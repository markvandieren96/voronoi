use crate::*;

#[derive(Clone)]
pub struct Group {
	pub src: SiteIndex,
	pub members: Vec<SiteIndex>,
}

pub fn graph_flood<S, P>(graph: &Graph<S>, predicate: P) -> Vec<Group>
where
	S: Site,
	P: Fn(&S, &S) -> bool,
{
	let mut taken_cells: Vec<SiteIndex> = Vec::new();
	let mut groups = Vec::new();

	for site in graph.sites.iter() {
		let index = site.index();
		if taken_cells.contains(&index) {
			continue;
		}

		let mut group = Group { src: index, members: Vec::new() };
		group.members.push(index);
		graph_flood_neighbors(site, &graph, &mut group, &mut taken_cells, &predicate);
		groups.push(group);
	}
	groups
}

fn graph_flood_neighbors<S, P>(site: &S, graph: &Graph<S>, group: &mut Group, taken_cells: &mut Vec<SiteIndex>, predicate: &P)
where
	S: Site,
	P: Fn(&S, &S) -> bool,
{
	if taken_cells.contains(&site.index()) {
		return;
	}

	for neighbor in graph.neighbors(site.index()) {
		if taken_cells.contains(&neighbor) || group.members.contains(&neighbor) {
			continue;
		}

		if predicate(&graph[group.src], &graph[neighbor]) {
			group.members.push(neighbor);
			graph_flood_neighbors(&graph[neighbor], graph, group, taken_cells, predicate);
			taken_cells.push(neighbor);
		}
	}
}
