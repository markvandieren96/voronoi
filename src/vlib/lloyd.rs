use crate::*;
use mcnum::Vec2f;

/// Computes the centroid of a polygon.
#[allow(clippy::ptr_arg)]
pub fn polygon_centroid(pts: &Vec<Point>) -> Point {
	let mut pt_sum = Point::new(0.0, 0.0);
	for pt in pts {
		pt_sum = *pt + pt_sum;
	}
	pt_sum * (1.0 / (pts.len() as f64))
}

/// Produces the Lloyd Relaxation of a set of points.
///
/// Each point is moved to the centroid of its Voronoi cell.
pub fn lloyd_relaxation(pts: Vec<Vec2f>, box_size: f64) -> Vec<Vec2f> {
	let pts = pts.iter().map(|pt| SiteRef::new(*pt)).collect();
	let voronoi = voronoi(pts, box_size, false);
	let faces = make_polygons(&voronoi);
	faces.iter().map(polygon_centroid).map(std::convert::Into::into).collect()
}
