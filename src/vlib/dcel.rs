use super::geometry::{segment_intersection, Segment};
use crate::*;
use mcnum::Vec2f;
use rstar::RTree;
use std::fmt;

#[derive(Default)]
pub struct DCEL {
	pub vertices: Vec<Vertex>,
	pub halfedges: Vec<HalfEdge>,
	pub faces: Vec<Face>,
}

impl std::ops::Index<EdgeIndex> for DCEL {
	type Output = HalfEdge;

	fn index(&self, index: EdgeIndex) -> &HalfEdge {
		&self.halfedges[index.0]
	}
}

impl std::ops::IndexMut<EdgeIndex> for DCEL {
	fn index_mut(&mut self, index: EdgeIndex) -> &mut HalfEdge {
		&mut self.halfedges[index.0]
	}
}

impl std::ops::Index<VertexIndex> for DCEL {
	type Output = Vertex;

	fn index(&self, index: VertexIndex) -> &Vertex {
		&self.vertices[index.0]
	}
}

impl std::ops::IndexMut<VertexIndex> for DCEL {
	fn index_mut(&mut self, index: VertexIndex) -> &mut Vertex {
		&mut self.vertices[index.0]
	}
}

impl std::ops::Index<FaceIndex> for DCEL {
	type Output = Face;

	fn index(&self, index: FaceIndex) -> &Face {
		&self.faces[index.0]
	}
}

impl std::ops::IndexMut<FaceIndex> for DCEL {
	fn index_mut(&mut self, index: FaceIndex) -> &mut Face {
		&mut self.faces[index.0]
	}
}

impl DCEL {
	/// Add two halfedges that are twins
	pub fn add_twins(&mut self) -> (EdgeIndex, EdgeIndex) {
		let mut he1 = HalfEdge::default();
		let mut he2 = HalfEdge::default();

		let start_index = self.halfedges.len();
		he1.twin = EdgeIndex(start_index + 1);
		he2.twin = EdgeIndex(start_index);
		self.halfedges.push(he1);
		self.halfedges.push(he2);
		(EdgeIndex(start_index), EdgeIndex(start_index + 1))
	}

	/// Get the origin of a halfedge by index
	pub fn get_origin(&self, edge: EdgeIndex) -> Point {
		let origin_ind = self[edge].origin;
		self[origin_ind].coordinates
	}

	/// Set the previous edge of all halfedges
	/// Assumes that the DCEL is well-formed.
	pub fn set_prev(&mut self) {
		let mut seen_edges = vec![false; self.halfedges.len()];
		for edge_ind in 0..self.halfedges.len() {
			if seen_edges[edge_ind] {
				continue;
			}
			let edge_ind = EdgeIndex(edge_ind);
			let mut current_ind = edge_ind;
			loop {
				let next_edge = self[current_ind].next;
				self[next_edge].prev = current_ind;
				current_ind = next_edge;
				seen_edges[current_ind.0] = true;
				if current_ind == edge_ind {
					break;
				}
			}
		}
	}

	fn remove_edge(&mut self, edge: EdgeIndex) {
		let edge_prev = self[edge].prev;
		let edge_next = self[edge].next;
		let twin = self[edge].twin;
		let twin_prev = self[twin].prev;
		let twin_next = self[twin].next;

		self[edge_prev].next = twin_next;
		self[edge_next].prev = twin_prev;
		self[twin_prev].next = edge_next;
		self[twin_next].prev = edge_prev;
		self[edge].alive = false;
		self[twin].alive = false;

		// MARKER: Kill edges
	}

	fn get_edges_around_vertex(&self, vertex: usize) -> Vec<EdgeIndex> {
		let mut result = vec![];
		let start_edge = self.vertices[vertex].incident_edge;
		let mut current_edge = start_edge;
		loop {
			result.push(current_edge);
			let current_twin = self[current_edge].twin;
			current_edge = self[current_twin].next;
			if current_edge == start_edge {
				break;
			}
		}
		result
	}

	/// Remove a vertex and all attached halfedges.
	/// Does not affect faces!!
	pub fn remove_vertex(&mut self, vertex: usize) {
		let vertex_edges = self.get_edges_around_vertex(vertex);
		for edge in vertex_edges {
			self.remove_edge(edge);
		}
		self.vertices[vertex].alive = false;
	}
}

impl fmt::Debug for DCEL {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		let mut vertices_disp = String::new();

		for (index, node) in self.vertices.iter().enumerate() {
			if node.alive {
				vertices_disp.push_str(format!("{}: {:?}\n", index, node).as_str());
			}
		}

		let mut faces_disp = String::new();

		for (index, node) in self.faces.iter().enumerate() {
			if node.alive {
				faces_disp.push_str(format!("{}: {}\n", index, node).as_str());
			}
		}

		let mut halfedges_disp = String::new();

		for (index, node) in self.halfedges.iter().enumerate() {
			if node.alive {
				halfedges_disp.push_str(format!("{}: {:?}\n", index, node).as_str());
			}
		}

		write!(f, "Vertices:\n{}\nFaces:\n{}\nHalfedges:\n{}", vertices_disp, faces_disp, halfedges_disp)
	}
}

/// Construct faces for a DCEL.
///
/// # Panics
///
/// This method will panic if the DCEL has any faces already.
pub fn add_faces(dcel: &mut DCEL, sites: Option<Vec<SiteRef>>) {
	if !dcel.faces.is_empty() {
		panic!("add_faces only works on DCELs with no faces");
	}
	let num_halfedges = dcel.halfedges.len();
	let mut seen_edges = vec![false; num_halfedges];

	for edge_index in 0..num_halfedges {
		let edge_index = EdgeIndex(edge_index);
		if seen_edges[edge_index.0] || !dcel[edge_index].alive {
			continue;
		}

		let face_index = FaceIndex(dcel.faces.len());
		let mut new_face = Face::new(edge_index, None); // MARKER: Face creation

		let mut face_vertices = Vec::new();

		let mut current_edge = edge_index;
		loop {
			seen_edges[current_edge.0] = true;

			face_vertices.push(dcel[dcel[current_edge].origin].coordinates);

			dcel[current_edge].face = face_index;
			current_edge = dcel[current_edge].next;
			if current_edge == edge_index {
				break;
			}
		}

		let centroid = super::lloyd::polygon_centroid(&face_vertices);
		new_face.centroid = Vec2f::new(centroid.x.into_inner() as f32, centroid.y.into_inner() as f32);
		dcel.faces.push(new_face);
	}

	if let Some(sites) = sites {
		let rtree = RTree::bulk_load(sites);

		// The last face in the list is the infinite/outside face, which doesn't correspond to any of the input sites
		for index in 0..dcel.faces.len() - 1 {
			dcel.faces[index].site = rtree.nearest_neighbor(&SiteRef::new(dcel.faces[index].centroid)).map(|neighbor| neighbor.t);
		}
	}
}

// does not handle the case where line goes through dcel vertex
/// Add a line segment to a DCEL.
///
/// Vertices and halfedges are constructed and mutated as necessary.
/// Faces are not affected. This should be used before add_faces.
pub fn add_line(seg: Segment, dcel: &mut DCEL) {
	let mut intersections = get_line_intersections(seg, dcel);
	intersections.sort_by(|a, b| a.0.cmp(&b.0));
	let start_pt = if seg[0] < seg[1] { seg[0] } else { seg[1] };
	let end_pt = if seg[0] < seg[1] { seg[1] } else { seg[0] };

	let (mut line_needs_next, mut line_needs_prev, _) = add_twins_from_pt(start_pt, dcel); // MARKER: Add twins
	dcel[line_needs_prev].next = line_needs_next;
	let prev_pt = start_pt;

	for (int_pt, this_cut_edge) in intersections {
		let (new_line_needs_next, new_line_needs_prev, new_pt_ind) = add_twins_from_pt(int_pt, dcel); // MARKER: Add twins
		dcel[line_needs_prev].origin = new_pt_ind;

		let mut cut_edge = this_cut_edge;
		if makes_left_turn(prev_pt, int_pt, dcel.get_origin(this_cut_edge)) {
			cut_edge = dcel[cut_edge].twin;
		}

		let old_cut_next = dcel[cut_edge].next;
		let old_cut_twin = dcel[cut_edge].twin;
		dcel[cut_edge].next = line_needs_prev;

		let cut_ext_ind = EdgeIndex(dcel.halfedges.len());
		let cut_ext_he = HalfEdge::new(new_pt_ind, old_cut_twin, EdgeIndex::default(), old_cut_next);

		dcel.halfedges.push(cut_ext_he);
		dcel[line_needs_next].next = cut_ext_ind;

		let old_twin_next = dcel[old_cut_twin].next;
		dcel[old_cut_twin].next = new_line_needs_next;

		let twin_ext_ind = EdgeIndex(dcel.halfedges.len());
		let twin_ext_he = HalfEdge::new(new_pt_ind, cut_edge, EdgeIndex::default(), old_twin_next);

		dcel.halfedges.push(twin_ext_he);
		dcel[new_line_needs_prev].next = twin_ext_ind;

		dcel[cut_edge].twin = twin_ext_ind;
		dcel[old_cut_twin].twin = cut_ext_ind;

		line_needs_next = new_line_needs_next;
		line_needs_prev = new_line_needs_prev;
	}

	dcel[line_needs_next].next = line_needs_prev;
	let end_vertex_ind = VertexIndex(dcel.vertices.len());
	let end_vertex = Vertex::new(end_pt, line_needs_prev);
	dcel.vertices.push(end_vertex);
	dcel[line_needs_prev].origin = end_vertex_ind;
}

/// Do the three points, in this order, make a left turn?
pub fn makes_left_turn(pt1: Point, pt2: Point, pt3: Point) -> bool {
	let x1 = pt1.x();
	let x2 = pt2.x();
	let x3 = pt3.x();
	let y1 = pt1.y();
	let y2 = pt2.y();
	let y3 = pt3.y();

	(x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1) > 0.
}

fn add_twins_from_pt(start_pt: Point, dcel: &mut DCEL) -> (EdgeIndex, EdgeIndex, VertexIndex) {
	let (twin1, twin2) = dcel.add_twins();

	let start_vertex = Vertex::new(start_pt, twin1);
	let start_vertex_ind = VertexIndex(dcel.vertices.len());
	dcel.vertices.push(start_vertex);

	dcel[twin1].origin = start_vertex_ind;

	(twin1, twin2, start_vertex_ind)
}

fn get_line_intersections(seg: Segment, dcel: &DCEL) -> Vec<(Point, EdgeIndex)> {
	let mut intersections = vec![];
	let mut seen_halfedges = vec![false; dcel.halfedges.len()];
	for (index, halfedge) in dcel.halfedges.iter().enumerate() {
		let twin = halfedge.twin;
		if seen_halfedges[index] || seen_halfedges[twin.0] || !halfedge.alive {
			continue;
		}
		let index = EdgeIndex(index);
		let this_seg = [dcel.get_origin(index), dcel.get_origin(twin)];
		let this_intersection = segment_intersection(seg, this_seg);
		if let Some(int_pt) = this_intersection {
			intersections.push((int_pt, index));
		}
		seen_halfedges[index.0] = true;
		seen_halfedges[twin.0] = true;
	}
	intersections
}

/// Constructs the line segments of the Voronoi diagram.
pub fn make_line_segments(dcel: &DCEL) -> Vec<Segment> {
	let mut result = vec![];
	for halfedge in &dcel.halfedges {
		if halfedge.origin.is_not_nil() && halfedge.next.is_not_nil() && halfedge.alive && dcel[halfedge.next].origin.is_not_nil() {
			result.push([dcel[halfedge.origin].coordinates, dcel.get_origin(halfedge.next)])
		}
	}
	result
}

/// Constructs the faces of the Voronoi diagram.
pub fn make_polygons(dcel: &DCEL) -> Vec<Vec<Point>> {
	let mut result = vec![];
	for face in &dcel.faces {
		if !face.alive {
			continue;
		}
		let mut this_poly = vec![];
		let start_edge = face.outer_component;
		let mut current_edge = start_edge;
		loop {
			this_poly.push(dcel.get_origin(current_edge));
			current_edge = dcel[current_edge].next;
			if current_edge == start_edge {
				break;
			}
		}
		result.push(this_poly);
	}

	// remove the outer face
	result.sort_by_key(|a| a.len());
	result.pop();

	result
}
