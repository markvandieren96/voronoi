use crate::*;
use mcnum::Vec2f;
use ordered_float::OrderedFloat;

pub type Segment = [Point; 2];

#[allow(clippy::many_single_char_names)]
pub fn segment_intersection(seg1: Segment, seg2: Segment) -> Option<Point> {
	let a = seg1[0];
	let c = seg2[0];
	let r = seg1[1] - a;
	let s = seg2[1] - c;

	let denom = r.cross(s);
	if denom == 0.0 {
		return None;
	}

	let numer_a = (c - a).cross(s);
	let numer_c = (c - a).cross(r);

	let t = numer_a / denom;
	let u = numer_c / denom;

	if !(0.0..=1.0).contains(&t) || u < 0.0 || u > 1.0 {
		return None;
	}

	Some(a + r * t)
}

pub fn circle_bottom(triple_site: TripleSite) -> Option<OrderedFloat<f64>> {
	let circle_center = circle_center(triple_site);
	circle_center?;
	let circle_center = circle_center.unwrap();

	let (_, _, p3) = triple_site;
	let x3 = p3.position.x;
	let y3 = p3.position.y;
	let x_cen = circle_center.x as f32;
	let y_cen = circle_center.y as f32;

	let r = ((x3 - x_cen) * (x3 - x_cen) + (y3 - y_cen) * (y3 - y_cen)).sqrt();

	Some(OrderedFloat::<f64>(f64::from(y_cen - r)))
}

pub fn circle_center(triple_site: TripleSite) -> Option<Vec2f> {
	let (p1, p2, p3) = triple_site;
	let x1 = p1.position.x;
	let x2 = p2.position.x;
	let x3 = p3.position.x;
	let y1 = p1.position.y;
	let y2 = p2.position.y;
	let y3 = p3.position.y;

	let c1 = x3 * x3 + y3 * y3 - x1 * x1 - y1 * y1;
	let c2 = x3 * x3 + y3 * y3 - x2 * x2 - y2 * y2;
	let a1 = -2.0 * (x1 - x3);
	let a2 = -2.0 * (x2 - x3);
	let b1 = -2.0 * (y1 - y3);
	let b2 = -2.0 * (y2 - y3);

	let numer = c1 * a2 - c2 * a1;
	let denom = b1 * a2 - b2 * a1;

	if denom == 0.0 {
		return None;
	}
	let y_cen = numer / denom;

	let x_cen = if a2 != 0.0 { (c2 - b2 * y_cen) / a2 } else { (c1 - b1 * y_cen) / a1 };

	Some(Vec2f::new(x_cen, y_cen))
}

// see http://www.kmschaal.de/Diplomarbeit_KevinSchaal.pdf, pg 27
pub fn breakpoints_converge(triple_site: TripleSite) -> bool {
	let (a, b, c) = triple_site;
	let ax = a.position.x;
	let ay = a.position.y;
	let bx = b.position.x;
	let by = b.position.y;
	let cx = c.position.x;
	let cy = c.position.y;

	(ay - by) * (bx - cx) > (by - cy) * (ax - bx)
}

#[allow(clippy::suspicious_operation_groupings)]
pub fn get_breakpoint_x(bp: &BreakPoint, yl: f32) -> f32 {
	let ax = bp.left_site.position.x;
	let bx = bp.right_site.position.x;
	let ay = bp.left_site.position.y;
	let by = bp.right_site.position.y;

	// shift frames
	let bx_s = bx - ax;
	let ay_s = ay - yl;
	let by_s = by - yl;

	let discrim = ay_s * by_s * ((ay_s - by_s) * (ay_s - by_s) + bx_s * bx_s);
	let numer = ay_s * bx_s - discrim.sqrt();
	let denom = ay_s - by_s;

	let mut x_bp = if denom != 0.0 { numer / denom } else { bx_s / 2. };
	x_bp += ax; // shift back to original frame

	x_bp
}

// to be done: handle py == yl case
pub fn get_breakpoint_y(bp: &BreakPoint, yl: f32) -> f32 {
	let px = bp.left_site.position.x;
	let py = bp.left_site.position.y;

	let bp_x = get_breakpoint_x(bp, yl);

	let numer = (px - bp_x) * (px - bp_x);
	let denom = 2. * (py - yl);

	numer / denom + (py + yl) / 2.0
}

#[cfg(test)]
mod tests {
	use crate::*;
	use mcnum::Vec2f;
	use ordered_float::OrderedFloat;

	#[test]
	fn simple_circle_center() {
		let circle_triple = (SiteRef::new(Vec2f::new(-1.0, 0.0)), SiteRef::new(Vec2f::new(0.0, 1.0)), SiteRef::new(Vec2f::new(1.0, 0.0)));
		assert_eq!(circle_center(circle_triple).unwrap(), Vec2f::new(0.0, 0.0));
	}

	#[test]
	fn simple_circle_bottom() {
		let circle_triple = (SiteRef::new(Vec2f::new(-1.0, 0.0)), SiteRef::new(Vec2f::new(0.0, 1.0)), SiteRef::new(Vec2f::new(1.0, 0.0)));
		assert_eq!(circle_bottom(circle_triple).unwrap(), OrderedFloat(-1.0));
	}

	#[test]
	fn degenerate_circle() {
		let circle_triple = (SiteRef::new(Vec2f::new(-1.0, 0.0)), SiteRef::new(Vec2f::new(1.0, 0.0)), SiteRef::new(Vec2f::new(0.0, 0.0)));
		assert_eq!(circle_bottom(circle_triple), None);
	}

	#[test]
	fn simple_segments_intersect() {
		let line1 = [Point::new(-1.0, 0.0), Point::new(1.0, 0.0)];
		let line2 = [Point::new(0.0, -1.0), Point::new(0.0, 1.0)];
		assert_eq!(segment_intersection(line1, line2), Some(Point::new(0.0, 0.0)));
	}

	#[test]
	fn tee_segments_intersect() {
		let line1 = [Point::new(-1.0, 0.0), Point::new(1.0, 0.0)];
		let line2 = [Point::new(0.0, 0.0), Point::new(0.0, 1.0)];
		assert_eq!(segment_intersection(line1, line2), Some(Point::new(0.0, 0.0)));
	}

	#[test]
	fn simple_segments_nonintersect() {
		let line1 = [Point::new(-1.0, 10.0), Point::new(1.0, 10.0)];
		let line2 = [Point::new(0.0, -1.0), Point::new(0.0, 1.0)];
		assert_eq!(segment_intersection(line1, line2), None);
	}
}
