use crate::*;
use mcnum::{Vec2f, Vec3f};

pub trait Site {
	fn index(&self) -> SiteIndex;

	fn position(&self) -> Vec2f;
	fn position_mut(&mut self) -> &mut Vec2f;

	fn color(&self) -> Vec3f;
}
