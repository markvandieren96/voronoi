use crate::*;
use mcnum::Vec2f;
use std::fmt;

#[derive(PartialEq, Eq)]
pub enum VertexType {
	Edge,
	Inner,
}

impl Default for VertexType {
	fn default() -> VertexType {
		VertexType::Inner
	}
}

pub struct Vertex {
	pub coordinates: Point,
	/// Some halfedge having this vertex as the origin
	pub incident_edge: EdgeIndex,
	pub alive: bool,
	pub vertex_type: VertexType,
	pub height: f32,
}

impl Vertex {
	pub fn new(position: Point, incident_edge: EdgeIndex) -> Vertex {
		Vertex {
			coordinates: position,
			incident_edge,
			alive: true,
			vertex_type: VertexType::default(),
			height: 0.0,
		}
	}
}

impl fmt::Debug for Vertex {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{:?}, edge: {:?}", self.coordinates, self.incident_edge)
	}
}

pub struct HalfEdge {
	pub origin: VertexIndex,
	pub twin: EdgeIndex,
	pub prev: EdgeIndex,
	pub next: EdgeIndex,
	pub face: FaceIndex,
	pub alive: bool,
}

impl Default for HalfEdge {
	fn default() -> HalfEdge {
		HalfEdge {
			origin: VertexIndex::default(),
			twin: EdgeIndex::default(),
			next: EdgeIndex::default(),
			face: FaceIndex::default(),
			prev: EdgeIndex::default(),
			alive: true,
		}
	}
}

impl fmt::Debug for HalfEdge {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "origin: {:?}, twin: {:?}, next: {:?}", self.origin, self.twin, self.next)
	}
}

impl HalfEdge {
	pub fn new(origin: VertexIndex, twin: EdgeIndex, prev: EdgeIndex, next: EdgeIndex) -> Self {
		HalfEdge {
			origin,
			twin,
			prev,
			next,
			face: FaceIndex::default(),
			alive: true,
		}
	}
}

#[derive(Debug)]
pub struct Face {
	pub outer_component: EdgeIndex, // index of halfedge
	pub alive: bool,
	pub site: Option<SiteIndex>,
	pub centroid: Vec2f,
}

impl fmt::Display for Face {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "outer: {:?}", self.outer_component)
	}
}

impl Face {
	/// Construct a new face, given an attached halfedge index
	pub fn new(edge: EdgeIndex, site: Option<SiteIndex>) -> Self {
		Face {
			outer_component: edge,
			alive: true,
			site,
			centroid: Vec2f::zero(),
		}
	}

	pub fn edges<'a>(&self, dcel: &'a DCEL) -> EdgeIterator<'a> {
		EdgeIterator::new(self.outer_component, dcel)
	}

	pub fn neighbors<'a>(&self, dcel: &'a DCEL) -> NeighborIterator<'a> {
		NeighborIterator::new(self.outer_component, dcel)
	}
}

#[derive(Debug)]
pub struct EdgeIterator<'a> {
	edge: EdgeIndex,
	start_edge: EdgeIndex,
	time_to_stop: bool,
	dcel: &'a DCEL,
}

impl<'a> EdgeIterator<'a> {
	pub fn new(edge: EdgeIndex, dcel: &'a DCEL) -> EdgeIterator<'a> {
		EdgeIterator {
			edge,
			start_edge: edge,
			time_to_stop: false,
			dcel,
		}
	}
}

impl<'a> Iterator for EdgeIterator<'a> {
	type Item = EdgeIndex;
	fn next(&mut self) -> Option<Self::Item> {
		if self.time_to_stop {
			return None;
		}
		let ret = self.edge;
		self.edge = self.dcel[self.edge].next;

		if self.start_edge == self.edge {
			self.time_to_stop = true;
		}

		Some(ret)
	}
}

#[derive(Debug)]
pub struct NeighborIterator<'a> {
	edge_iter: EdgeIterator<'a>,
	dcel: &'a DCEL,
}

impl<'a> NeighborIterator<'a> {
	pub fn new(edge: EdgeIndex, dcel: &'a DCEL) -> NeighborIterator<'a> {
		NeighborIterator {
			edge_iter: EdgeIterator::new(edge, dcel),
			dcel,
		}
	}
}

impl<'a> Iterator for NeighborIterator<'a> {
	type Item = FaceIndex;
	fn next(&mut self) -> Option<Self::Item> {
		match self.edge_iter.next() {
			Some(edge) => {
				let twin = self.dcel[edge].twin;
				let twin_face = self.dcel[twin].face;
				Some(twin_face)
			}
			None => None,
		}
	}
}
