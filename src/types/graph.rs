use crate::*;
use mcnum::Vec2f;
use rstar::RTree;
use std::ops;

pub type VertexRef = TreeNode<VertexIndex>;

pub struct Graph<S: Site> {
	pub min: Vec2f,
	pub max: Vec2f,
	pub dim: u32,
	pub sites: Vec<S>,
	pub dcel: DCEL,
	tree: RTree<SiteRef>,
	pub vertex_tree: RTree<VertexRef>,
}

impl<S: Site> ops::Index<SiteIndex> for Graph<S> {
	type Output = S;

	fn index(&self, index: SiteIndex) -> &S {
		&self.sites[index.0]
	}
}

impl<S: Site> ops::IndexMut<SiteIndex> for Graph<S> {
	fn index_mut(&mut self, index: SiteIndex) -> &mut S {
		&mut self.sites[index.0]
	}
}

impl<S: Site> ops::Index<EdgeIndex> for Graph<S> {
	type Output = HalfEdge;

	fn index(&self, index: EdgeIndex) -> &HalfEdge {
		&self.dcel[index]
	}
}

impl<S: Site> ops::IndexMut<EdgeIndex> for Graph<S> {
	fn index_mut(&mut self, index: EdgeIndex) -> &mut HalfEdge {
		&mut self.dcel[index]
	}
}

impl<S: Site> ops::Index<VertexIndex> for Graph<S> {
	type Output = Vertex;

	fn index(&self, index: VertexIndex) -> &Vertex {
		&self.dcel[index]
	}
}

impl<S: Site> ops::IndexMut<VertexIndex> for Graph<S> {
	fn index_mut(&mut self, index: VertexIndex) -> &mut Vertex {
		&mut self.dcel[index]
	}
}

impl<S: Site> ops::Index<FaceIndex> for Graph<S> {
	type Output = Face;

	fn index(&self, index: FaceIndex) -> &Face {
		&self.dcel[index]
	}
}

impl<S: Site> ops::IndexMut<FaceIndex> for Graph<S> {
	fn index_mut(&mut self, index: FaceIndex) -> &mut Face {
		&mut self.dcel[index]
	}
}

fn relax_n(mut points: Vec<Vec2f>, relax_count: u32, dim: u32) -> Vec<Vec2f> {
	for _ in 0..relax_count {
		points = lloyd_relaxation(points, f64::from(dim));
	}
	points
}

fn convert_to_sites(points: Vec<Vec2f>) -> Vec<SiteRef> {
	let mut index = 0;
	points
		.iter()
		.map(|point| {
			let s = SiteRef { position: *point, t: SiteIndex(index) };
			index += 1;
			s
		})
		.collect()
}

impl<S: Site> Graph<S> {
	pub fn new<F>(dim: u32, mut points: Vec<Vec2f>, relax_count: u32, func: F) -> Graph<S>
	where
		F: Fn(SiteIndex, Vec2f) -> S,
	{
		points = relax_n(points, relax_count, dim);
		let vor_sites = convert_to_sites(points);
		let sites: Vec<S> = vor_sites.iter().map(|site| func(site.t, site.position)).collect();
		let dcel = voronoi(vor_sites.clone(), f64::from(dim), true);
		let rtree = RTree::bulk_load(vor_sites);

		let vertex_refs = dcel
			.vertices
			.iter()
			.enumerate()
			.map(|(index, vertex)| VertexRef {
				position: vertex.coordinates.into(),
				t: VertexIndex(index),
			})
			.collect();
		let vertex_tree = RTree::bulk_load(vertex_refs);

		Graph {
			min: Vec2f::zero(),
			max: Vec2f::new_xy(dim as f32),
			dim,
			sites,
			dcel,
			tree: rtree,
			vertex_tree,
		}
	}

	pub fn closest_site(&self, position: Vec2f) -> SiteIndex {
		let nearest = self.tree.nearest_neighbor(&SiteRef::new(position)).expect("Could not find nearest site!");
		nearest.t
	}

	pub fn closest_vertex(&self, position: Vec2f) -> VertexIndex {
		let nearest = self.vertex_tree.nearest_neighbor(&VertexRef::new(position)).expect("Could not find nearest vertex!");
		nearest.t
	}

	pub fn get_face(&self, site: SiteIndex) -> Option<usize> {
		for (index, face) in self.dcel.faces.iter().enumerate() {
			if face.site == Some(site) {
				return Some(index);
			}
		}
		None
	}

	pub fn neighbors(&self, site: SiteIndex) -> Vec<SiteIndex> {
		match self.get_face(site) {
			Some(face_index) => {
				let face = &self.dcel.faces[face_index];
				let mut neighbors = Vec::new();

				let mut current_edge = face.outer_component;
				loop {
					let twin_index = self.dcel[current_edge].twin;
					let twin = &self.dcel[twin_index];
					if let Some(neighbor) = self.dcel[twin.face].site {
						neighbors.push(neighbor);
					}

					current_edge = self.dcel[current_edge].next;
					if current_edge == face.outer_component {
						break;
					}
				}

				neighbors
			}
			None => Vec::new(),
		}
	}
}
