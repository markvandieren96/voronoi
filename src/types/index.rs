use crate::TreeNode;
use serde::{Deserialize, Serialize};

pub const NIL: usize = !0; //15
pub const NIL_DEAD: usize = NIL - 1; //14

pub const NIL_EDGE: usize = NIL - 2; //13
pub const NIL_MARKER: usize = NIL - 3; //12
pub const NIL_MARKER2: usize = NIL - 4; //11
pub const NIL_MARKER3: usize = NIL - 5; //10
pub const NIL_MARKER4: usize = NIL - 6; //9

pub type SiteRef = TreeNode<SiteIndex>;
pub type TripleSite = (SiteRef, SiteRef, SiteRef);

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct EdgeIndex(pub usize);

impl Default for EdgeIndex {
	fn default() -> EdgeIndex {
		EdgeIndex(NIL)
	}
}

impl EdgeIndex {
	pub fn is_nil(self) -> bool {
		self.0 == NIL
	}

	pub fn is_not_nil(self) -> bool {
		self.0 != NIL
	}
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct VertexIndex(pub usize);

impl Default for VertexIndex {
	fn default() -> VertexIndex {
		VertexIndex(NIL)
	}
}

impl VertexIndex {
	pub fn is_nil(self) -> bool {
		self.0 == NIL
	}

	pub fn is_not_nil(self) -> bool {
		self.0 != NIL
	}
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct FaceIndex(pub usize);

impl Default for FaceIndex {
	fn default() -> FaceIndex {
		FaceIndex(NIL)
	}
}

impl FaceIndex {
	pub fn is_nil(self) -> bool {
		self.0 == NIL
	}

	pub fn is_not_nil(self) -> bool {
		self.0 != NIL
	}
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct SiteIndex(pub usize);

impl Default for SiteIndex {
	fn default() -> SiteIndex {
		SiteIndex(NIL)
	}
}

impl SiteIndex {
	pub fn is_nil(self) -> bool {
		self.0 == NIL
	}

	pub fn is_not_nil(self) -> bool {
		self.0 != NIL
	}
}
