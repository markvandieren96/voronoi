use mcnum::Vec2f;
use std::fmt;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct TreeNode<T> {
	pub position: Vec2f,
	pub t: T,
}

impl<T> TreeNode<T>
where
	T: Default,
{
	pub fn new(position: Vec2f) -> TreeNode<T> {
		TreeNode { position, t: T::default() }
	}
}

impl<T> rstar::Point for TreeNode<T>
where
	T: fmt::Debug + Default + Copy + PartialEq,
{
	type Scalar = f32;
	const DIMENSIONS: usize = 2;

	fn generate(generator: impl Fn(usize) -> Self::Scalar) -> Self {
		TreeNode {
			position: Vec2f::new(generator(0), generator(1)),
			t: T::default(),
		}
	}

	fn nth(&self, index: usize) -> Self::Scalar {
		self.position[index]
	}

	fn nth_mut(&mut self, index: usize) -> &mut Self::Scalar {
		&mut self.position[index]
	}
}
